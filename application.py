import redis
from flask import Flask, request, json
from db_file import expenseTable
from db_file import db
from db_file import createDatabase


app = Flask(__name__)


@app.route('/v1/expenses/', methods=['POST'])
def newEntry():
    session = createDatabase()
    db.create_all()
    newObject = request.get_json(force=True)
    entry = expenseTable(newObject['name'], newObject['email'], newObject['category'], newObject['description'],
                           newObject['link'], newObject['estimated_costs'], newObject['submit_date'],
                  'pending', '')

    db.session.add(entry)
    db.session.commit()
    db.session.flush()
    return json.dumps([{'id': entry.id, 'name': entry.name, 'email': entry.email,
                    'category': entry.category, 'description': entry.description,
                    'link': entry.link, 'estimated_costs': entry.estimated_costs,
                    'submit_date': entry.submit_date, 'status': entry.status,
                    'decision_date': entry.decision_date}]), 201

@app.route('/v1/expenses/<int:expense_id>', methods=['GET', 'PUT', 'DELETE'])
def checkEntry(expense_id):
    check = expenseTable.query.filter_by(id=expense_id).first_or_404()


    if(request.method == 'DELETE'):

        db.session.delete(check)
        db.session.commit()
        return json.dumps([{'status':True}]), 204



    if(request.method == 'PUT'):
        newEntry = request.get_json(force=True)
        check.estimated_costs = newEntry['estimated_costs']

        db.session.commit()
        return json.dumps([{'status':True}]), 202

    if (request.method == 'GET'):
        return json.dumps([{'id': check.id, 'name': check.name, 'email': check.email,
                        'category': check.category, 'description': check.description,
                        'link': check.link, 'estimated_costs': check.estimated_costs,
                        'submit_date': check.submit_date, 'status': check.status,
                        'decision_date': check.decision_date}])

if __name__ == '__main__':

    r_server = redis.Redis("127.0.0.1")
    r_server.lrem("PORTS", "6001", 0)
    r_server.rpush("PORTS", "6001")

    app.run(host="127.0.0.1", port=6001, debug=False)
